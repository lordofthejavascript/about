export const game = {
  namespaced: true,
  state () {
    return {
      bulletCount: 0,
      asteroidsCount: 0,
      level: 1,
      isStart: false,
      isEnd: false,
      isPause: false,
    }
  },
  mutations: {
    setAsteroidsCount (state, payload) {
      state.asteroidsCount = payload
    },
    setBulletCount (state, payload) {
      state.bulletCount = payload
    },
    setLevel (state, payload) {
      state.level = payload
    },
    setEnd (state, payload) {
      state.isEnd = payload
    },
    setPause (state, payload) {
      state.isPause = payload
    },
    setStart (state, payload) {
      state.isStart = payload
    },

  },
  // actions: {
  //     setScrollIndex ({ state, commit }, { deltaIndex, key, stateKey }) {
  //     },
  // },
  // getters: {
  //     getCurrentContent: (state) => state.screens.data && state.screens.data[state.screens.currentIndex].content,
  // },
}
