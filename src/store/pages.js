export const pages = {
  namespaced: true,
  state () {
    return {
      scrollValue: {
        z: 20,
        x: 0,
        y: 0,
      },
      isNext: false,
      videoPreloader: {
        isStarted: false,
        slideScrollIndex: false,
        isVideoEnded: false,
      },
      screens: {
        data: null,
        currentIndex: 0,
        components: [
          {
            stopKey: 400,
            name: 'MainScreen',
            className: 'screen-main',
            route: '/',
          },
          {
            stopKey: null,
            name: 'SecondScreen',
            className: 'screen-second',
            route: '/about',
          },
        ],
      },
    }
  },
  mutations: {
    setZvalue (state, payload) {
      state.scrollValue.z = payload
    },
    setCurrentIndexValue (state, payload) {
      state.scrollValue.x = payload
    },
    setCurrentIndex (state, index) {
      state.screens.currentIndex = index
    },
    setData (state, payload) {
      state.screens.data = payload
    },
    setPreloaderState (state, key) {
      state.videoPreloader.isStarted = key
    },
    setNextState (state, key) {
      state.isNext = key
    },
    setDefaultScrollValue (state, payload) {
      payload.forEach((item) => {
        for (const key in state.scrollValue) {
          if (item === key) {
            state.scrollValue[key] = 0
          }
        }
      })
    },
  },
  actions: {
    setScrollIndex ({ state, commit }, { deltaIndex, key, stateKey }) {
      const stopKey = (state.screens.components[state.screens.currentIndex]) ? state.screens.components[state.screens.currentIndex].stopKey : null
      // if (!stopKey) {
      //   return
      // }
      if (key) {
        (deltaIndex > 0) ? state.scrollValue.x -= 0.05 : state.scrollValue.x += 0.05
      } else {
        (deltaIndex > 0) ? state.scrollValue.x += 0.05 : state.scrollValue.x -= 0.05
      }
      state.scrollValue.x = parseFloat(state.scrollValue.x)
      if (stopKey && (parseFloat(state.scrollValue.x) * 100) > stopKey) {
        commit('setNextState', stateKey)
      }
    },
  },
  getters: {
    getCurrentContent: (state) => state.screens.data && state.screens.data[state.screens.currentIndex].content,
    setScreen (state) {
      return state.screens.components[state.screens.currentIndex]
    },
    translateIndex (state) {
      return state.scrollValue.z - (parseFloat(state.scrollValue.x.toFixed(2)) * 50)
    },
  },
}
