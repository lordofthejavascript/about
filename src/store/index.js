import { pages } from './pages'
import { game } from './game'
import { createStore } from 'vuex'

const indexStore = createStore({
  modules: {
    pages: pages,
    game: game,
  },
})

export default indexStore
