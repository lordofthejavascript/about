import { Modules } from '../index'

class OnScrollClass extends Modules {
  constructor (app) {
    super(app)
    this.app = this.context.config.globalProperties
    this.touchStart = null
    this.touchTrigger = null
    this.invert = false
  }

  setLayout (name) {
  }

  onScrollEvent (evt) {
    let delta = 0
    let touchMovie = null

    if (evt.type === 'touchstart') {
      this.touchStart = evt.touches[0].clientY
    }

    if (evt.type === 'touchmove') {
      if (!this.touchTrigger) {
        delta = (this.touchStart < touchMovie) ? -1 : 1
      } else {
        delta = (this.touchTrigger < touchMovie) ? -1 : 1
      }
      touchMovie = evt.changedTouches[0].clientY
      delta = (this.touchTrigger < touchMovie) ? -1 : 1
      this.app.$store.dispatch('pages/setScrollIndex', {
        deltaIndex: delta, key: this.invert, stateKey: true,
      })
      setTimeout(() => {
        this.touchTrigger = evt.changedTouches[0].clientY
      }, 0)
    } else {
      delta = evt.deltaY || evt.detail || evt.wheelDelta
      this.app.$store.dispatch('pages/setScrollIndex', {
        deltaIndex: delta, key: this.invert, stateKey: true,
      })
    }
  }

  init () {
  }
}

export const Scroll = (app) => {
  return new OnScrollClass(app)
}
