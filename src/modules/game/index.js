import { Modules } from '../index'
import Phaser from 'phaser'

class GameClass extends Modules {
  constructor (app) {
    super(app)
    this.app = this.context.config.globalProperties
    this.game = {}
    this.container = {
      w: 0,
      h: 0,
      id: '',
    }
  }

  destroy () {
    // if (Object.keys(this.game).length !== 0 && this.app.route.path !== '/game') {
    this.game.destroy(true)
    // }
  }

  init (containerId, { w, h }, isPaused) {
    const context = this.app
    this.container.id = containerId
    this.container.w = w
    this.container.h = h
    this.game = new Phaser.Game({
      type: Phaser.WEBGL,
      parent: containerId,
      width: (window.innerWidth >= 800) ? 800 : w,
      height: (window.innerHeight >= 600) ? 600 : h,
      backgroundColor: '#000000',
      physics: {
        default: 'arcade',
        arcade: {
          gravity: {
            y: 300,
          },
        },
      },
      dom: {
        createContainer: true,
      },
      scene: {
        preload: function () {
          this.load.image('space', 'game/slides/1.jpg')
          this.load.image('falcon', '/game/icons/falcon.png')
          this.load.image('asteroid-1', '/game/icons/asteroid_1.png')
          this.load.image('asteroid-2', '/game/icons/asteroid_2.png')
          this.load.image('asteroid-3', '/game/icons/asteroid_3.png')
          this.load.spritesheet('explode', '/game/icons/explode.png', {
            frameWidth: 128, frameHeight: 128,
          })
          this.load.image('blaster', '/game/icons/blaster.png')
        },
        create: function () {
          this.cursors = null
          this.speed = null
          this.asteroids = null
          this.bulletCount = 10
          this.gameOver = false
          this.asteroids = this.physics.add.group({
            immovable: true,
            allowGravity: false,
          })
          this.lastFired = 0

          const destroyAsteroid = (asteroid, bullet) => {
            this.explose.create(asteroid.x, asteroid.y, 'explode')
            this.explose.children.entries.forEach((item) => {
              item.anims.play('boom', false)
              setTimeout(() => {
                item.destroy()
              }, 1000)
            })
            this.bulletCount = (this.bulletCount > 0) ? this.bulletCount - 1 : 0
            context.$store.commit('game/setBulletCount', this.bulletCount)
            asteroid.destroy()
            bullet.destroy()
          }

          const hintAsteroid = (ship, asteroid) => {
            this.explose = this.add.sprite(ship.x, ship.y, 'explode')
            this.explose.anims.play('boom', false)
            this.explose.on('animationcomplete', () => {
              this.explose.destroy()
            })
            context.$store.commit('game/setPause', true)
            context.$store.commit('game/setEnd', true)
            asteroid.destroy()
            this.timer.remove()
            this.time.paused = true
            this.gameOver = true
          }

          this.bullet = new Phaser.Class({

            Extends: Phaser.GameObjects.Image,

            initialize: function Bullet (scene) {
              Phaser.GameObjects.Image.call(this, scene, 0, 0, 'blaster')
              this.speed = Phaser.Math.GetSpeed(500, 1)
            },

            fire: function (x, y) {
              this.setPosition(x, y - 50)
              this.setActive(true)
              this.setVisible(true)
            },

            update: function (time, delta) {
              this.y -= this.speed * delta

              if (this.y < -50) {
                this.setActive(false)
                this.setVisible(false)
              }
            },
          })

          this.bullets = this.physics.add.group({
            classType: this.bullet,
            immovable: true,
            allowGravity: false,
            maxSize: 1,
            runChildUpdate: true,
          })

          // this.explosions = this.add.group();

          this.starfield = this.add.tileSprite(0, 0, 1600, 1200, 'space')
          this.speed = Phaser.Math.GetSpeed(300, 1)
          this.timer = this.time.addEvent({
            delay: 1000,
            callback: () => {
              const x = Phaser.Math.Between(0, 800)
              this.asteroids.create(x, 0, `asteroid-${parseInt(Math.random() * (4 - 1) + 1)}`)
            },
            repeat: 200,
          })
          this.ship = this.physics.add.sprite(400, 500, 'falcon')
          this.ship.setCollideWorldBounds(true)
          this.cursors = this.input.keyboard.createCursorKeys()
          this.speed = Phaser.Math.GetSpeed(300, 1)
          this.physics.add.collider(this.ship, this.asteroids, hintAsteroid, null, this)
          this.physics.add.collider(this.bullets, this.asteroids, destroyAsteroid, null, this)

          this.anims.create({
            key: 'boom',
            frames: this.anims.generateFrameNumbers('explode', {
              start: 0, end: 30,
            }),
            frameRate: 20,
            repeat: 0,
          })

          this.explose = this.add.group()
          context.$store.commit('game/setPause', false)

          this.timer.paused = isPaused
        },
        update: function (time, delta) {
          if (this.gameOver) {
            return
          }

          this.starfield.tilePositionY -= 2
          context.$store.commit('game/setAsteroidsCount', this.asteroids.children.entries.length)

          switch (this.asteroids.children.entries.length) {
            case 20:
              this.timer.delay = 600
              this.bulletCount = 10
              context.$store.commit('game/setBulletCount', this.bulletCount)
              context.$store.commit('game/setLevel', 2)
              break
            case 50:
              this.timer.delay = 400
              this.bulletCount = 20
              context.$store.commit('game/setBulletCount', this.bulletCount)
              context.$store.commit('game/setLevel', 3)
              break
            case 150:
              this.timer.delay = 300
              this.bulletCount = 10
              context.$store.commit('game/setBulletCount', this.bulletCount)
              context.$store.commit('game/setLevel', 4)
              break
            case 200:
              context.$store.commit('game/setEnd', true)
              context.$store.commit('game/setPause', true)
              break
          }

          this.asteroids.children.entries.forEach((item) => {
            item.angle -= parseFloat(Math.random() * (1.9 - 0.5) + 0.5).toFixed(1)

            switch (item.texture.key) {
              case 'asteroid-3':
                item.setDisplaySize(180, 178)
                break
              case 'asteroid-2':
                item.setDisplaySize(130, 142)
                break
              case 'asteroid-1':
                item.setDisplaySize(295, 265)
                break
              default:
                item.setDisplaySize(180, 178)
                break
            }

            item.y += this.speed * delta
          })

          if (this.cursors.left.isDown) {
            this.ship.x -= this.speed * delta
          }

          if (this.cursors.right.isDown) {
            this.ship.x += this.speed * delta
          }

          if (this.cursors.up.isDown) {
            this.ship.y -= this.speed * delta
          }

          if (this.cursors.up.isDown) {
            this.ship.y -= this.speed * delta
          }

          if (this.cursors.space.isDown && time > this.lastFired) {
            const bullet = this.bullets.get()
            // if (bullet && this.bulletCount !== 0) {
            if (bullet) {
              bullet.fire(this.ship.x, this.ship.y)
              this.lastFired = time + 50
            }
          }
        },
      },
    })
  }

  start () {
    this.game.scene.scenes[0].timer.paused = false
    this.app.$store.commit('game/setStart', true)
    this.app.$store.commit('game/setPause', false)
    // this.app.$store.commit('game/setEnd', false)
  }

  restart () {
    this.destroy()
    this.init(this.container.id, {
      w: this.container.w,
      h: this.container.h,
    }, false)
    this.app.$store.commit('game/setPause', true)
    this.app.$store.commit('game/setEnd', false)
  }
}
export const Game = (app) => {
  return new GameClass(app)
}
