import { Modules } from '../index'

class ApiClass extends Modules {
  constructor (app) {
    super(app)
    this.app = this.context.config.globalProperties
    this.app.axios.interceptors.response.use((response) => {
      return response
    }, (error) => {
      console.log(error.response, 'interceptor_error')
      return Promise.reject(error)
    })
  }

  async userAuthorization (userId) {
    try {
      return await this.app.axios.post('/api/users/authorization/', {
        fingerprint: userId,
      })
    } catch (e) {}
  }

  async getScreensData () {
    try {
      return await this.app.axios.get('/static/pages.json').then((item) => item.data)
    } catch (e) {}
  }

  init () {
  }
}

export const Api = (app) => {
  return new ApiClass(app)
}
