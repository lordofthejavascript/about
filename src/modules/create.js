
import { Api } from './api'
import { Game } from './game'
import * as isMobile from 'mobile-device-detect'
import FingerprintJS from '@fingerprintjs/fingerprintjs'
import { ElNotification } from 'element-plus'

export const createModules = (app) => {
  app.config.globalProperties.$nf = ElNotification
  app.config.globalProperties.$fp = FingerprintJS
  app.config.globalProperties.$mobile = isMobile
  app.config.globalProperties.$modules = {
    api: new Api(app),
    game: new Game(app),
  }
}
