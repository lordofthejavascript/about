import { watch } from 'vue'

export const useCommon = () => {
  let touchStart = null
  let touchTrigger = null
  const invert = false

  const onWatchIndex = (getter, { scroll, scale, elements }) => {
    watch(getter, (e, y) => {
      scroll.value = (e > y) ? scroll.value += 0.1 : scroll.value -= 0.1
      scale.value = Math.abs(scroll.value)
      if (scale.value > 2) {
        scroll.value = scale.value = 0
        elements.index.value += 1
      }
    })
  }

  const onScrollValue = (evt, context) => {
    let delta = 0
    let touchMovie = null

    if (evt.type === 'touchstart') {
      touchStart = evt.touches[0].clientY
    }

    if (evt.type === 'touchmove') {
      if (!touchTrigger) {
        delta = (touchStart < touchMovie) ? -1 : 1
      } else {
        delta = (touchTrigger < touchMovie) ? -1 : 1
      }
      touchMovie = evt.changedTouches[0].clientY
      delta = (touchTrigger < touchMovie) ? -1 : 1
      context.$store.dispatch('pages/setScrollIndex', {
        deltaIndex: delta, key: invert, stateKey: true,
      })
      setTimeout(() => {
        touchTrigger = evt.changedTouches[0].clientY
      }, 0)
    } else {
      delta = evt.deltaY || evt.detail || evt.wheelDelta
      context.$store.dispatch('pages/setScrollIndex', {
        deltaIndex: delta, key: invert, stateKey: true,
      })
    }
  }

  return {
    onScrollValue,
    onWatchIndex,
  }
}
