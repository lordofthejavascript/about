import * as Vue from 'vue'
import App from './App.vue'
import indexStore from './store/index'
import vuescroll from 'vue-scroll'
import Vue3TouchEvents from 'vue3-touch-events'
import axios from 'axios'
import VueAxios from 'vue-axios'
import { routesCreate } from './router'
import { createModules } from './modules/create'
import { ElButton, ElUpload } from 'element-plus'
import BootstrapIcon from '@dvuckovic/vue3-bootstrap-icons'
// import 'element-plus/dist/index.css'

const app = Vue.createApp(App)
const router = routesCreate(app)

app.component(ElUpload.name, ElUpload)
app.component(ElButton.name, ElButton)
app.component('BootstrapIcon', BootstrapIcon)

app.use(VueAxios, axios)
app.use(Vue3TouchEvents)
app.use(indexStore)
app.use(vuescroll)
app.use(router)
createModules(app)
app.mount('#app')
