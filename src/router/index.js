import * as VueRouter from 'vue-router'
import MainScreen from '../components/screens/MainScreen'
import SecondScreen from '../components/screens/SecondScreen'
import Game from '../components/screens/Game'

const routes = [
  {
    path: '/',
    component: MainScreen,
  },
  {
    path: '/about',
    component: SecondScreen,
  },
  {
    path: '/game',
    component: Game,
  },
]

export const routesCreate = () => {
  return VueRouter.createRouter({
    history: VueRouter.createWebHashHistory(),
    routes,
  })
}
